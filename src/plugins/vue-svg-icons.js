import VueSvgIcons from 'v-svg-icons'
import Vue from 'vue'

Vue.component('icon', VueSvgIcons)
