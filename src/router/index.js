import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomePage',
    meta: { onlyAuthUser: false },
    component: () => import('@/pages/homepage.vue')
  },
  {
    path: '/auth',
    name: 'Auth',
    meta: { onlyAuthUser: false },
    component: () => import('@/pages/auth.vue'),
    children: [
      {
        path: 'login',
        name: 'Login',
        meta: { onlyAuthUser: false },
        component: () => import('@/components/elements/auth/login.vue')
      },
      {
        path: 'register',
        name: 'Register',
        meta: { onlyAuthUser: false },
        component: () => import('@/components/elements/auth/register.vue')
      }
    ]
  },
  {
    path: '/profile',
    name: 'Profile',
    meta: { onlyAuthUser: true },
    component: () => import('@/pages/profile.vue'),
    children: [
      {
        path: ':page',
        name: 'ProfileHome',
        meta: { onlyAuthUser: true },
        component: () => import('@/components/elements/profile/profile.vue'),
        children: [
          {
            path: ':subPage',
            name: 'ProfileHome',
            meta: { onlyAuthUser: true },
            component: () => import('@/components/elements/profile/profile.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/consignment',
    name: 'Consignment',
    meta: { onlyAuthUser: false },
    component: () => import('@/pages/consignment.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
