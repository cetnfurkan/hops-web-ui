import axios from 'axios'

const baseURL = 'http'

const axiosInstance = axios.create({
  baseURL
})

export default axiosInstance
